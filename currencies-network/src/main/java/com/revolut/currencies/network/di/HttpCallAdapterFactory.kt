package com.revolut.currencies.network.di

import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.Single
import retrofit2.Call
import retrofit2.CallAdapter
import retrofit2.HttpException
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import java.io.IOException
import java.lang.reflect.Type

internal class HttpCallAdapterFactory(
  private val factory: RxJava2CallAdapterFactory
) : CallAdapter.Factory() {

  @Suppress("UNCHECKED_CAST")
  override fun get(returnType: Type, annotations: Array<Annotation>, retrofit: Retrofit): CallAdapter<Any, Any>? =
    factory
      .get(returnType, annotations, retrofit)
      ?.let { CallAdapterWrapper(it as CallAdapter<Any, Any>) }

  private class CallAdapterWrapper(
    private val adapter: CallAdapter<Any, Any>
  ) : CallAdapter<Any, Any> {

    override fun responseType(): Type =
      adapter.responseType()

    override fun adapt(call: Call<Any>): Any =
      adapter
        .adapt(call)
        .let {
          when (it) {
            is Completable -> it.onErrorResumeNext { throwable -> Completable.error(getThrowable(throwable)) }
            is Single<*> -> it.onErrorResumeNext { throwable -> Single.error(getThrowable(throwable)) }
            is Observable<*> -> it.onErrorResumeNext { throwable: Throwable -> Observable.error(getThrowable(throwable)) }
            else -> throw RuntimeException("Call Type ${it.javaClass} not supported")
          }
        }

    private fun getThrowable(throwable: Throwable): Throwable {
      if (throwable is HttpException) {
        val response = throwable.response()
        return ApiException(response.code(), response.message(), response.errorBody(), throwable)
      }

      if (throwable is IOException) {
        return NetworkException(throwable)
      }

      return throwable
    }
  }

  companion object {
    fun create(factory: RxJava2CallAdapterFactory): CallAdapter.Factory = HttpCallAdapterFactory(factory)
  }
}