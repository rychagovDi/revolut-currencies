package com.revolut.currencies.network.di

import okhttp3.ResponseBody

class ApiException(
  val responseCode: Int,
  val responseMessage: String,
  val responseBody: ResponseBody?,
  cause: Throwable
) : Exception(cause)

class NetworkException(
  cause: Throwable
) : Exception(cause)