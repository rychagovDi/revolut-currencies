package com.revolut.currencies.network.di

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Module
object NetworkModule {
  private const val BASE_URL = "https://revolut.duckdns.org"
  private const val DATE_FORMAT = "yyyy-MM-dd"

  @JvmStatic
  @Provides
  @Singleton
  fun provideLogingInterceptor(): HttpLoggingInterceptor =
    HttpLoggingInterceptor().apply {
      level = HttpLoggingInterceptor.Level.BODY
    }

  @JvmStatic
  @Provides
  @Singleton
  fun provideOkHttpClient(loggingInterceptor: HttpLoggingInterceptor): OkHttpClient =
    OkHttpClient.Builder()
      .connectTimeout(30, TimeUnit.SECONDS)
      .readTimeout(30, TimeUnit.SECONDS)
      .writeTimeout(30, TimeUnit.SECONDS)
      .addInterceptor(loggingInterceptor)
      .build()

  @JvmStatic
  @Provides
  @Singleton
  fun provideGson(): Gson =
    GsonBuilder()
      .setDateFormat(DATE_FORMAT)
      .create()

  @JvmStatic
  @Provides
  @Singleton
  fun provideRetrofit(
    okHttpClient: OkHttpClient,
    gson: Gson
  ): Retrofit =
    Retrofit.Builder()
      .baseUrl(BASE_URL)
      .client(okHttpClient)
      .addConverterFactory(GsonConverterFactory.create(gson))
      .addCallAdapterFactory(HttpCallAdapterFactory.create(RxJava2CallAdapterFactory.createAsync()))
      .build()
}