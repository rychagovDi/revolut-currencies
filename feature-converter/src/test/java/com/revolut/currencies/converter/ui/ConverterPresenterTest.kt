package com.revolut.currencies.converter.ui

import com.nhaarman.mockitokotlin2.*
import com.revolut.currencies.converter.R
import com.revolut.currencies.converter.data.Currency
import com.revolut.currencies.network.di.ApiException
import com.revolut.currencies.network.di.NetworkException
import io.reactivex.Observable
import org.junit.ClassRule
import org.junit.Test

class ConverterPresenterTest {

  companion object {
    @ClassRule
    @JvmField
    val schedulers = RxImmediateSchedulerRule()
  }

  private val modelMock = mock<Converter.Model> {
    on { currencies() } doReturn Observable.just(emptyList())
  }
  private val viewMock = mock<Converter.View>()
  private val presenter: Converter.Presenter = ConverterPresenter(modelMock)

  @Test
  fun `bind() calls ConverterModel#bind() `() {
    presenter.bind(viewMock)
    verify(modelMock).bind()
    presenter.unbind()
  }

  @Test
  fun `bind() calls ConverterModel#currencies()`() {
    presenter.bind(viewMock)
    verify(modelMock).currencies()
    presenter.unbind()
  }

  @Test
  fun `unbind() calls ConverterModel#unbind()`() {
    presenter.unbind()
    verify(modelMock).unbind()
  }

  @Test
  fun `bind() shows progress and hides placeholder|keyboard`() {
    presenter.bind(viewMock)
    verify(viewMock, atLeast(1)).showProgress()
    verify(viewMock, atLeast(1)).hidePlaceholder()
    verify(viewMock, atLeast(1)).hideKeyboard()
    presenter.unbind()
  }

  @Test
  fun `items shown after success loading`() {
    presenter.bind(viewMock)
    verify(viewMock, timeout(500)).showItems(emptyList(), false)
    presenter.unbind()
  }

  @Test
  fun `api placeholder shown after api error`() {
    val modelErrorApi = mock<Converter.Model> {
      on { currencies() } doReturn Observable.error(ApiException(0, "", null, Throwable()))
    }
    val presenterErrorApi = ConverterPresenter(modelErrorApi)

    presenterErrorApi.bind(viewMock)
    verify(viewMock, timeout(500)).showPlaceholder(R.string.error_api)
    presenterErrorApi.unbind()
  }

  @Test
  fun `network placeholder shown after network error`() {
    val modelErrorNetwork = mock<Converter.Model> {
      on { currencies() } doReturn Observable.error(NetworkException(Throwable()))
    }
    val presenterErrorNetwork = ConverterPresenter(modelErrorNetwork)

    presenterErrorNetwork.bind(viewMock)
    verify(viewMock, timeout(500)).showPlaceholder(R.string.error_network)
    presenterErrorNetwork.unbind()
  }

  @Test
  fun `common placeholder shown after any other error`() {
    val modelError = mock<Converter.Model> {
      on { currencies() } doReturn Observable.error(Throwable())
    }
    val presenterError = ConverterPresenter(modelError)
    presenterError.bind(viewMock)
    verify(viewMock, timeout(500)).showPlaceholder(R.string.error_common)
    presenterError.unbind()
  }


  @Test
  fun `hide keyboard on scroll`() {
    presenter.bind(viewMock)
    clearInvocations(viewMock)
    presenter.onCurrenciesScrolled(1, 1)
    verify(viewMock).hideKeyboard()
    presenter.unbind()
  }

  @Test
  fun `don't hide keyboard without scroll`() {
    presenter.bind(viewMock)
    clearInvocations(viewMock)
    presenter.onCurrenciesScrolled(0, 0)
    verify(viewMock, never()).hideKeyboard()
    presenter.unbind()
  }

  @Test
  fun `onCurrencyItemClicked() invoke model#setBaseCurrency|#setBaseValue`() {
    presenter.bind(viewMock)
    presenter.onCurrencyItemClicked(CurrencyItem(Currency.RUB, 0, 0, 0, 100f))
    verify(modelMock).setBaseCurrency(Currency.RUB)
    verify(modelMock).setBaseValue(100f)
    presenter.unbind()
  }

  @Test
  fun `onCurrencyValueChanged() invoke model##setBaseValue`() {
    presenter.bind(viewMock)
    presenter.onCurrencyValueChanged("100.00")
    verify(modelMock).setBaseValue(100.00f)
    presenter.unbind()
  }

  @Test
  fun `onRetryClick() start currencies loading`() {
    presenter.bind(viewMock)
    clearInvocations(modelMock)
    presenter.onRetryClicked()
    verify(modelMock).currencies()
    presenter.unbind()
  }
}