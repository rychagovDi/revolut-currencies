package com.revolut.currencies.converter.ui

import com.minyushov.adapter.ModularItem
import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import com.revolut.currencies.converter.data.ConverterRepository
import com.revolut.currencies.converter.data.Currency
import com.revolut.currencies.converter.data.Rate
import io.reactivex.Completable
import io.reactivex.Single
import io.reactivex.observers.TestObserver
import org.junit.Test
import org.mockito.Mockito
import java.util.concurrent.TimeUnit

class ConverterModelTest {

  private val valueStub = 100f
  private val currencyStub = Currency.RUB

  private val baseRubRates = listOf(
    Rate(Currency.RUB, 1f),
    Rate(Currency.AUD, 2f),
    Rate(Currency.BGN, 3f)
  )
  private val baseAudRates = listOf(
    Rate(Currency.AUD, 2f),
    Rate(Currency.RUB, 1f),
    Rate(Currency.BGN, 3f)
  )

  private val repositoryMock = mock<ConverterRepository> {
    on { baseCurrency(currencyStub) } doReturn Single.just(currencyStub)
    on { baseValue(Mockito.anyFloat()) } doReturn Single.just(valueStub)
    on { latestRates(Currency.RUB) } doReturn Single.just(baseRubRates)
    on { latestRates(Currency.AUD) } doReturn Single.just(baseAudRates)
    on { saveBaseCurrency(Currency.RUB) } doReturn Completable.complete()
    on { saveBaseCurrency(Currency.AUD) } doReturn Completable.complete()
    on { saveBaseValue(Mockito.anyFloat()) } doReturn Completable.complete()
  }

  private val model: Converter.Model = ConverterModel(repositoryMock)

  @Test
  fun `bind() calls ConverterRepository#baseCurrency()`() {
    model.bind()
    verify(repositoryMock).baseCurrency(currencyStub)
    model.unbind()
  }

  @Test
  fun `bind() calls ConverterRepository#baseValue()`() {
    model.bind()
    verify(repositoryMock).baseValue(Mockito.anyFloat())
    model.unbind()
  }

  @Test
  fun `currencies() emit items after bind() call`() {
    val testObserver = TestObserver.create<List<ModularItem>>()
    model.currencies().subscribe(testObserver)
    model.bind()
    testObserver.await(300, TimeUnit.MILLISECONDS)
    testObserver.assertValue { true } // simply check that items were emitted
    testObserver.dispose()
    model.unbind()
  }

  @Test
  fun `setBaseCurrency() force currencies() emit new items`() {
    val testObserver = TestObserver.create<List<ModularItem>>()
    model.currencies().subscribe(testObserver)
    model.bind()
    testObserver.awaitCount(1)
    model.setBaseCurrency(Currency.AUD)
    testObserver.awaitCount(2)
    testObserver.dispose()
    model.unbind()
  }

  @Test
  fun `setBaseValue() force currencies() emit new items`() {
    val testObserver = TestObserver.create<List<ModularItem>>()
    model.currencies().subscribe(testObserver)
    model.bind()
    testObserver.awaitCount(1)
    model.setBaseValue(1f)
    testObserver.awaitCount(2)
    testObserver.dispose()
    model.unbind()
  }
}