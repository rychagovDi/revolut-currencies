package com.revolut.currencies.converter.data

data class Rate(
  val currency: Currency,
  val rate: Float
)