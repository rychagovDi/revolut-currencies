package com.revolut.currencies.converter.ui

import com.minyushov.adapter.ModularItem
import com.revolut.currencies.converter.R
import com.revolut.currencies.network.di.ApiException
import com.revolut.currencies.network.di.NetworkException
import com.revolut.currencies.utils.addToComposite
import com.revolut.currencies.utils.applyUiSchedulers
import io.reactivex.disposables.CompositeDisposable

internal class ConverterPresenter(
  private val model: Converter.Model
) : Converter.Presenter {

  private var view: Converter.View? = null
  private val disposable = CompositeDisposable()

  private var pendingScrollToTop: Boolean = false

  override fun bind(view: Converter.View) {
    this.view = view
    model.bind()

    loadCurrencies()
  }

  override fun unbind() {
    model.unbind()
    disposable.clear()
    view = null
  }

  override fun onCurrenciesScrolled(dx: Int, dy: Int) {
    if (dx != 0 || dy != 0) {
      view?.hideKeyboard()
    }
  }

  override fun onCurrencyItemClicked(item: CurrencyItem) {
    view?.hideKeyboard()

    model.setBaseCurrency(item.currency)
    model.setBaseValue(item.value)
    pendingScrollToTop = true
  }

  override fun onCurrencyValueChanged(rawValue: String) {
    val value = rawValue.toFloatOrNull() ?: 0f
    model.setBaseValue(value)
  }

  override fun onRetryClicked() {
    loadCurrencies()
  }

  private fun loadCurrencies() {
    view?.showProgress()
    view?.hideKeyboard()
    view?.hidePlaceholder()

    model
      .currencies()
      .applyUiSchedulers()
      .subscribe(
        { onCurrenciesUpdated(it) },
        { onCurrenciesUpdateError(it) }
      )
      .addToComposite(disposable)
  }

  private fun onCurrenciesUpdated(items: List<ModularItem>) {
    view?.hideProgress()
    view?.hidePlaceholder()

    view?.showItems(items, pendingScrollToTop)
    pendingScrollToTop = false
  }

  private fun onCurrenciesUpdateError(throwable: Throwable) {
    val title = when (throwable) {
      is ApiException -> R.string.error_api
      is NetworkException -> R.string.error_network
      else -> R.string.error_common
    }

    view?.hideKeyboard()
    view?.hideProgress()
    view?.showPlaceholder(title)
  }
}