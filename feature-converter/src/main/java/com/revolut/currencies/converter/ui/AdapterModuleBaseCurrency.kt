package com.revolut.currencies.converter.ui

import android.content.Context
import android.text.Editable
import android.text.TextWatcher
import android.util.AttributeSet
import android.view.ViewGroup
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.ext.recyclerview.widget.ViewHolder
import com.minyushov.adapter.AdapterModule
import com.minyushov.adapter.ModularItem
import com.revolut.currencies.converter.R
import com.revolut.currencies.converter.data.Currency
import com.revolut.currencies.converter.utils.ValueFormatter
import java.util.*

data class BaseCurrencyItem(
  val currency: Currency,
  @DrawableRes
  val flag: Int,
  @StringRes
  val iso: Int,
  @StringRes
  val name: Int,
  val value: Float
) : ModularItem


class BaseCurrencyItemView
@JvmOverloads
constructor(
  context: Context,
  attrs: AttributeSet? = null,
  style: Int = 0
) : ConstraintLayout(
  context,
  attrs,
  style
) {
  private val flag: ImageView
  private val iso: TextView
  private val name: TextView
  private val value: EditText

  private var onValueChangedCallback: ((String) -> Unit)? = null
  private val onValueChangedListener: TextWatcher
  private val formatter: ValueFormatter = ValueFormatter("^([0-9]*[.]?)[0-9]{0,2}")

  init {
    layoutParams = LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT)
    inflate(context, R.layout.i_base_currency, this)

    setBackgroundResource(R.color.white)

    flag = findViewById(R.id.base_currency_flag)
    iso = findViewById(R.id.base_currency_iso)
    name = findViewById(R.id.base_currency_name)
    value = findViewById(R.id.base_currency_value)

    onValueChangedListener = object : TextWatcher {
      override fun afterTextChanged(s: Editable) {
        val (result, isFormatted) = formatter.format(s)
        if (isFormatted) {
          value.removeTextChangedListener(this)
          value.setText(result)
          value.setSelection(result.length)
          value.addTextChangedListener(this)
        }
        onValueChangedCallback?.invoke(result.toString())
      }

      override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) = Unit
      override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) = Unit
    }

    if (isInEditMode) {
      setFlag(R.drawable.flag_russia)
      setIso(R.string.currency_rub_iso)
      setName(R.string.currency_rub_name)
      setValue(1234.56F)
    }
  }

  fun setFlag(@DrawableRes flagRes: Int) {
    flag.setImageResource(flagRes)
  }

  fun setIso(@StringRes isoRes: Int) {
    iso.setText(isoRes)
  }

  fun setName(@StringRes nameRes: Int) {
    name.setText(nameRes)
  }

  fun setValue(value: Float) {
    this.value.setText(String.format(Locale.US, "%.2f", value))
  }

  fun setValueChangedCallback(onValueChanged: ((String) -> Unit)?) {
    this.onValueChangedCallback = onValueChanged
  }

  fun bindValueChangedListener() {
    value.addTextChangedListener(onValueChangedListener)
  }

  fun unbindValueChangedListener() {
    value.removeTextChangedListener(onValueChangedListener)
  }
}


class AdapterModuleBaseCurrency(
  private val onValueChanged: (String) -> Unit
) : AdapterModule<ViewHolder<BaseCurrencyItemView>, BaseCurrencyItem>() {

  private object Payload {
    const val FLAG = 1
    const val ISO = 1 shl 1
    const val NAME = 1 shl 2
  }

  override fun onCreateViewHolder(parent: ViewGroup): ViewHolder<BaseCurrencyItemView> =
    ViewHolder(BaseCurrencyItemView(parent.context).apply {
      setValueChangedCallback(onValueChanged)
    })

  override fun onBindViewHolder(holder: ViewHolder<BaseCurrencyItemView>, item: BaseCurrencyItem) {
    holder.view.apply {
      setFlag(item.flag)
      setIso(item.iso)
      setName(item.name)
      setValue(item.value)
    }
  }

  override fun onBindViewHolder(holder: ViewHolder<BaseCurrencyItemView>, item: BaseCurrencyItem, payloads: List<Any>) {
    holder.view.apply {
      val payload = payloads.getOrNull(0) as? Int ?: return@apply
      if (payload and Payload.FLAG == Payload.FLAG) {
        setFlag(item.flag)
      }
      if (payload and Payload.ISO == Payload.ISO) {
        setIso(item.iso)
      }
      if (payload and Payload.NAME == Payload.NAME) {
        setName(item.name)
      }
      // We don't set `value` there because it changed by user's interaction
    }
  }

  override fun onViewAttachedToWindow(holder: ViewHolder<BaseCurrencyItemView>) {
    holder.view.bindValueChangedListener()
  }

  override fun onViewDetachedFromWindow(holder: ViewHolder<BaseCurrencyItemView>) {
    holder.view.unbindValueChangedListener()
  }

  override fun areItemsTheSame(oldItem: BaseCurrencyItem, newItem: BaseCurrencyItem): Boolean =
    oldItem.currency == newItem.currency

  override fun areContentsTheSame(oldItem: BaseCurrencyItem, newItem: BaseCurrencyItem): Boolean =
    oldItem.flag == newItem.flag
        && oldItem.iso == newItem.iso
        && oldItem.name == newItem.name

  override fun getChangePayload(oldItem: BaseCurrencyItem, newItem: BaseCurrencyItem): Any? {
    var payload = 0
    if (oldItem.flag != newItem.flag) {
      payload = payload or Payload.FLAG
    }
    if (oldItem.iso != newItem.iso) {
      payload = payload or Payload.ISO
    }
    if (oldItem.name != newItem.name) {
      payload = payload or Payload.NAME
    }

    return payload
  }
}