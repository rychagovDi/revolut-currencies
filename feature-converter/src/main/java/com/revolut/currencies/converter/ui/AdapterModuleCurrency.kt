package com.revolut.currencies.converter.ui

import android.content.Context
import android.util.AttributeSet
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.ext.recyclerview.widget.ViewHolder
import com.minyushov.adapter.AdapterModule
import com.minyushov.adapter.ModularItem
import com.minyushov.adapter.plugins.ItemClickPlugin
import com.revolut.currencies.converter.R
import com.revolut.currencies.converter.data.Currency
import java.util.*

data class CurrencyItem(
  val currency: Currency,
  @DrawableRes
  val flag: Int,
  @StringRes
  val iso: Int,
  @StringRes
  val name: Int,
  val value: Float
) : ModularItem

class CurrencyItemView
@JvmOverloads
constructor(
  context: Context,
  attrs: AttributeSet? = null,
  style: Int = 0
) : ConstraintLayout(
  context,
  attrs,
  style
) {
  private val flag: ImageView
  private val iso: TextView
  private val name: TextView
  private val value: TextView

  init {
    layoutParams = LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT)
    inflate(context, R.layout.i_currency, this)

    setBackgroundResource(R.color.white)

    flag = findViewById(R.id.currency_flag)
    iso = findViewById(R.id.currency_iso)
    name = findViewById(R.id.currency_name)
    value = findViewById(R.id.currency_value)

    if (isInEditMode) {
      setFlag(R.drawable.flag_russia)
      setIso(R.string.currency_rub_iso)
      setName(R.string.currency_rub_name)
      setValue(1234.56F)
    }
  }

  fun setFlag(@DrawableRes flagRes: Int) {
    flag.setImageResource(flagRes)
  }

  fun setIso(@StringRes isoRes: Int) {
    iso.setText(isoRes)
  }

  fun setName(@StringRes nameRes: Int) {
    name.setText(nameRes)
  }

  fun setValue(value: Float) {
    this.value.text = String.format(Locale.US, "%.2f", value)
  }
}

class AdapterModuleCurrency(
  private val onClick: (CurrencyItem) -> Unit
) : AdapterModule<ViewHolder<CurrencyItemView>, CurrencyItem>(), ItemClickPlugin<CurrencyItem> {

  private object Payload {
    const val FLAG = 1
    const val ISO = 1 shl 1
    const val NAME = 1 shl 2
    const val VALUE = 1 shl 3
  }

  override fun onCreateViewHolder(parent: ViewGroup): ViewHolder<CurrencyItemView> =
    ViewHolder(CurrencyItemView(parent.context))

  override fun onBindViewHolder(holder: ViewHolder<CurrencyItemView>, item: CurrencyItem) {
    holder.view.apply {
      setFlag(item.flag)
      setIso(item.iso)
      setName(item.name)
      setValue(item.value)
    }
  }

  override fun onBindViewHolder(holder: ViewHolder<CurrencyItemView>, item: CurrencyItem, payloads: List<Any>) {
    holder.view.apply {
      val payload = payloads.getOrNull(0) as? Int ?: return@apply
      if (payload and Payload.FLAG == Payload.FLAG) {
        setFlag(item.flag)
      }
      if (payload and Payload.ISO == Payload.ISO) {
        setIso(item.iso)
      }
      if (payload and Payload.NAME == Payload.NAME) {
        setName(item.name)
      }
      if (payload and Payload.VALUE == Payload.VALUE) {
        setValue(item.value)
      }
    }
  }

  override fun onItemClicked(item: CurrencyItem, position: Int) {
    onClick.invoke(item)
  }

  override fun areItemsTheSame(oldItem: CurrencyItem, newItem: CurrencyItem): Boolean =
    oldItem.currency == newItem.currency

  override fun areContentsTheSame(oldItem: CurrencyItem, newItem: CurrencyItem): Boolean =
    oldItem.flag == newItem.flag
        && oldItem.iso == newItem.iso
        && oldItem.name == newItem.name
        && oldItem.value == newItem.value

  override fun getChangePayload(oldItem: CurrencyItem, newItem: CurrencyItem): Any? {
    var payload = 0
    if (oldItem.flag != newItem.flag) {
      payload = payload or Payload.FLAG
    }
    if (oldItem.iso != newItem.iso) {
      payload = payload or Payload.ISO
    }
    if (oldItem.name != newItem.name) {
      payload = payload or Payload.NAME
    }
    if (oldItem.value != newItem.value) {
      payload = payload or Payload.VALUE
    }

    return payload
  }
}