package com.revolut.currencies.converter.ui

import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.minyushov.adapter.DiffDataSource
import com.minyushov.adapter.ModularAdapter
import com.minyushov.adapter.ModularItem
import com.revolut.currencies.converter.R
import com.revolut.currencies.utils.hideKeyboard
import dagger.android.AndroidInjection
import javax.inject.Inject

class ConverterActivity
  : AppCompatActivity(), Converter.View {

  private lateinit var progress: View
  private lateinit var placeholder: View
  private lateinit var placeholderTitle: TextView
  private lateinit var placeholderButton: Button
  private lateinit var currenciesRecycler: RecyclerView

  private val dataSource = DiffDataSource<ModularItem>()

  @Inject
  lateinit var presenter: Converter.Presenter

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.a_converter)

    AndroidInjection.inject(this)

    progress = findViewById(R.id.converter_progress)
    placeholder = findViewById(R.id.converter_placeholder_error)
    placeholderTitle = findViewById(R.id.converter_placeholder_error_title)
    placeholderButton = findViewById(R.id.converter_placeholder_error_button)
    placeholderButton.setOnClickListener {
      presenter.onRetryClicked()
    }

    currenciesRecycler = findViewById(R.id.converter_currencies)
    currenciesRecycler.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
    currenciesRecycler.addOnScrollListener(object : RecyclerView.OnScrollListener() {
      override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
        presenter.onCurrenciesScrolled(dx, dy)
      }
    })

    val adapter = ModularAdapter<RecyclerView.ViewHolder, ModularItem>()
    adapter.registerModule(AdapterModuleCurrency(
      onClick = { presenter.onCurrencyItemClicked(it) }
    ))
    adapter.registerModule(AdapterModuleBaseCurrency(
      onValueChanged = { presenter.onCurrencyValueChanged(it) }
    ))
    adapter.setDataSource(dataSource)
    currenciesRecycler.adapter = adapter
  }

  override fun onStart() {
    super.onStart()
    presenter.bind(this)
  }

  override fun onStop() {
    super.onStop()
    presenter.unbind()
  }

  override fun showItems(items: List<ModularItem>, scrollToTop: Boolean) {
    dataSource.submitItems(items) {
      if (scrollToTop) {
        currenciesRecycler.scrollToPosition(0)
      }
    }
  }

  override fun showProgress() {
    progress.visibility = View.VISIBLE
  }

  override fun hideProgress() {
    progress.visibility = View.GONE
  }

  override fun showPlaceholder(title: Int) {
    placeholderTitle.setText(title)
    placeholder.visibility = View.VISIBLE
  }

  override fun hidePlaceholder() {
    placeholder.visibility = View.GONE
  }

  override fun hideKeyboard() {
    currenciesRecycler.hideKeyboard()
  }
}
