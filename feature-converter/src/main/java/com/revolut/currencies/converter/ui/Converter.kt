package com.revolut.currencies.converter.ui

import androidx.annotation.StringRes
import com.minyushov.adapter.ModularItem
import com.revolut.currencies.converter.data.Currency
import io.reactivex.Observable

interface Converter {
  interface Model {
    fun bind()
    fun unbind()

    fun currencies(): Observable<List<ModularItem>>

    fun setBaseCurrency(currency: Currency)
    fun setBaseValue(value: Float)
  }

  interface View {
    fun showItems(items: List<ModularItem>, scrollToTop: Boolean)

    fun showProgress()
    fun hideProgress()

    fun showPlaceholder(@StringRes title: Int)
    fun hidePlaceholder()

    fun hideKeyboard()
  }

  interface Presenter {
    fun bind(view: View)
    fun unbind()

    fun onCurrenciesScrolled(dx: Int, dy: Int)

    fun onCurrencyItemClicked(item: CurrencyItem)
    fun onCurrencyValueChanged(rawValue: String)

    fun onRetryClicked()
  }
}