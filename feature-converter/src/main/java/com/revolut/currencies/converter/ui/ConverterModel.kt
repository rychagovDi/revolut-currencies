package com.revolut.currencies.converter.ui

import com.jakewharton.rxrelay2.BehaviorRelay
import com.minyushov.adapter.ModularItem
import com.revolut.currencies.converter.R
import com.revolut.currencies.converter.data.ConverterRepository
import com.revolut.currencies.converter.data.Currency
import com.revolut.currencies.converter.data.Rate
import com.revolut.currencies.utils.addToComposite
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import java.util.concurrent.TimeUnit

internal class ConverterModel(
  private val converterRepository: ConverterRepository
) : Converter.Model {

  private val baseCurrency = BehaviorRelay.create<Currency>()
  private val baseValue = BehaviorRelay.create<Float>()

  private val disposable = CompositeDisposable()

  override fun bind() {
    converterRepository
      .baseCurrency(default = Currency.RUB)
      .subscribeOn(Schedulers.io())
      .subscribe(baseCurrency)
      .addToComposite(disposable)

    converterRepository
      .baseValue(default = 100f)
      .subscribeOn(Schedulers.io())
      .subscribe(baseValue)
      .addToComposite(disposable)
  }

  override fun unbind() {
    disposable.clear()
  }

  override fun currencies(): Observable<List<ModularItem>> =
    baseCurrency
      .observeOn(Schedulers.io())
      .switchMap { baseCurrency ->
        timer(1, TimeUnit.SECONDS)
          .switchMap {
            converterRepository
              .latestRates(baseCurrency)
              .flatMapObservable { rates ->
                baseValue
                  .observeOn(Schedulers.io())
                  .map { baseValue ->
                    mutableListOf<ModularItem>().apply {
                      add(baseCurrency.toBaseCurrencyItem(baseValue))
                      addAll(rates.map { it.toCurrencyItem(baseValue) })
                    }
                  }
              }
          }
      }

  override fun setBaseCurrency(currency: Currency) {
    converterRepository
      .saveBaseCurrency(currency)
      .subscribeOn(Schedulers.io())
      .subscribe { baseCurrency.accept(currency) }
      .addToComposite(disposable)
  }

  override fun setBaseValue(value: Float) {
    converterRepository
      .saveBaseValue(value)
      .subscribeOn(Schedulers.io())
      .subscribe { baseValue.accept(value) }
      .addToComposite(disposable)
  }

  private fun timer(period: Long, unit: TimeUnit) =
    Observable.merge(
      Observable.just(0L),
      Observable.interval(period, unit)
    )

  private fun Currency.toBaseCurrencyItem(baseValue: Float): ModularItem =
    withResources { flag, iso, name ->
      BaseCurrencyItem(
        currency = this,
        flag = flag,
        iso = iso,
        name = name,
        value = baseValue
      )
    }

  private fun Rate.toCurrencyItem(baseValue: Float): ModularItem =
    currency.withResources { flag, iso, name ->
      CurrencyItem(
        currency = currency,
        flag = flag,
        iso = iso,
        name = name,
        value = baseValue * rate
      )
    }

  private inline fun <T> Currency.withResources(
    transform: (Int, Int, Int) -> T
  ) = when (this) {
    Currency.AUD -> transform.invoke(
      R.drawable.flag_australia,
      R.string.currency_aud_iso,
      R.string.currency_aud_name
    )
    Currency.BGN -> transform.invoke(
      R.drawable.flag_bulgaria,
      R.string.currency_bgn_iso,
      R.string.currency_bgn_name
    )
    Currency.BRL -> transform.invoke(
      R.drawable.flag_brazil,
      R.string.currency_brl_iso,
      R.string.currency_brl_name
    )
    Currency.CAD -> transform.invoke(
      R.drawable.flag_canada,
      R.string.currency_cad_iso,
      R.string.currency_cad_name
    )
    Currency.CNF -> transform.invoke(
      R.drawable.flag_switzerland,
      R.string.currency_chf_iso,
      R.string.currency_chf_name
    )
    Currency.CNY -> transform.invoke(
      R.drawable.flag_china,
      R.string.currency_cny_iso,
      R.string.currency_cny_name
    )
    Currency.CZK -> transform.invoke(
      R.drawable.flag_czech_republic,
      R.string.currency_czk_iso,
      R.string.currency_czk_name
    )
    Currency.DKK -> transform.invoke(
      R.drawable.flag_denmark,
      R.string.currency_dkk_iso,
      R.string.currency_dkk_name
    )
    Currency.EUR -> transform.invoke(
      R.drawable.flag_european_union,
      R.string.currency_eur_iso,
      R.string.currency_eur_name
    )
    Currency.GBP -> transform.invoke(
      R.drawable.flag_england,
      R.string.currency_gbp_iso,
      R.string.currency_gbp_name
    )
    Currency.HKD -> transform.invoke(
      R.drawable.flag_hong_kong,
      R.string.currency_hkd_iso,
      R.string.currency_hkd_name
    )
    Currency.HRK -> transform.invoke(
      R.drawable.flag_croatia,
      R.string.currency_hrk_iso,
      R.string.currency_hrk_name
    )
    Currency.HUF -> transform.invoke(
      R.drawable.flag_hungary,
      R.string.currency_huf_iso,
      R.string.currency_huf_name
    )
    Currency.IDR -> transform.invoke(
      R.drawable.flag_indonesia,
      R.string.currency_idr_iso,
      R.string.currency_idr_name
    )
    Currency.ILS -> transform.invoke(
      R.drawable.flag_israel,
      R.string.currency_ils_iso,
      R.string.currency_ils_name
    )
    Currency.INR -> transform.invoke(
      R.drawable.flag_india,
      R.string.currency_inr_iso,
      R.string.currency_inr_name
    )
    Currency.ISK -> transform.invoke(
      R.drawable.flag_iceland,
      R.string.currency_isk_iso,
      R.string.currency_isk_name
    )
    Currency.JPY -> transform.invoke(
      R.drawable.flag_japan,
      R.string.currency_jpy_iso,
      R.string.currency_jpy_name
    )
    Currency.KRW -> transform.invoke(
      R.drawable.flag_south_korea,
      R.string.currency_krw_iso,
      R.string.currency_krw_name
    )
    Currency.MXN -> transform.invoke(
      R.drawable.flag_mexico,
      R.string.currency_mxn_iso,
      R.string.currency_mxn_name
    )
    Currency.MYR -> transform.invoke(
      R.drawable.flag_malaysia,
      R.string.currency_myr_iso,
      R.string.currency_myr_name
    )
    Currency.NOK -> transform.invoke(
      R.drawable.flag_norway,
      R.string.currency_nok_iso,
      R.string.currency_nok_name
    )
    Currency.NZD -> transform.invoke(
      R.drawable.flag_new_zealand,
      R.string.currency_nzd_iso,
      R.string.currency_nzd_name
    )
    Currency.PHP -> transform.invoke(
      R.drawable.flag_philippines,
      R.string.currency_php_iso,
      R.string.currency_php_name
    )
    Currency.PLN -> transform.invoke(
      R.drawable.flag_republic_of_poland,
      R.string.currency_pln_iso,
      R.string.currency_pln_name
    )
    Currency.RON -> transform.invoke(
      R.drawable.flag_romania,
      R.string.currency_ron_iso,
      R.string.currency_ron_name
    )
    Currency.RUB -> transform.invoke(
      R.drawable.flag_russia,
      R.string.currency_rub_iso,
      R.string.currency_rub_name
    )
    Currency.SEK -> transform.invoke(
      R.drawable.flag_sweden,
      R.string.currency_sek_iso,
      R.string.currency_sek_name
    )
    Currency.SGD -> transform.invoke(
      R.drawable.flag_singapore,
      R.string.currency_sgd_iso,
      R.string.currency_sgd_name
    )
    Currency.THB -> transform.invoke(
      R.drawable.flag_thailand,
      R.string.currency_thb_iso,
      R.string.currency_thb_name
    )
    Currency.TRY -> transform.invoke(
      R.drawable.flag_turkey,
      R.string.currency_try_iso,
      R.string.currency_try_name
    )
    Currency.USD -> transform.invoke(
      R.drawable.flag_united_states_of_america,
      R.string.currency_usd_iso,
      R.string.currency_usd_name
    )
    Currency.ZAR -> transform.invoke(
      R.drawable.flag_south_africa,
      R.string.currency_zar_iso,
      R.string.currency_zar_name
    )
  }
}