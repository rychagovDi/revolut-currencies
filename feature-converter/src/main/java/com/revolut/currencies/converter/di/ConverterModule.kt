package com.revolut.currencies.converter.di

import android.content.SharedPreferences
import com.revolut.currencies.api.Api
import com.revolut.currencies.converter.data.ConverterPreferences
import com.revolut.currencies.converter.data.ConverterRepository
import com.revolut.currencies.converter.data.DefaultConverterPreferences
import com.revolut.currencies.converter.data.DefaultConverterRepository
import com.revolut.currencies.converter.ui.Converter
import com.revolut.currencies.converter.ui.ConverterModel
import com.revolut.currencies.converter.ui.ConverterPresenter
import dagger.Module
import dagger.Provides

@Module
object ConverterModule {

  @JvmStatic
  @Provides
  fun provideConverterPreferences(sharedPreferences: SharedPreferences): ConverterPreferences = DefaultConverterPreferences(sharedPreferences)

  @JvmStatic
  @Provides
  fun provideCurrenciesRepository(
    preferences: ConverterPreferences,
    api: Api
  ): ConverterRepository = DefaultConverterRepository(preferences, api)

  @JvmStatic
  @Provides
  fun provideConverterModel(repository: ConverterRepository): Converter.Model = ConverterModel(repository)

  @JvmStatic
  @Provides
  fun provideConverterPresenter(model: Converter.Model): Converter.Presenter = ConverterPresenter(model)
}