package com.revolut.currencies.converter.utils

import java.util.regex.Pattern

class ValueFormatter(
  pattern: String
) {

  data class Result(
    val result: CharSequence,
    val formatted: Boolean
  )

  private val regex = Pattern.compile(pattern).toRegex()

  fun format(input: CharSequence): Result {
    val result = regex.find(input)?.value ?: input
    val formatted = result != input.toString()

    return Result(result, formatted)
  }
}