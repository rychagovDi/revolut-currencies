package com.revolut.currencies.converter.data

import com.revolut.currencies.api.Api
import com.revolut.currencies.api.data.LatestRates
import io.reactivex.Completable
import io.reactivex.Single

interface ConverterRepository {
  fun latestRates(currency: Currency): Single<List<Rate>>

  fun baseCurrency(default: Currency): Single<Currency>
  fun baseValue(default: Float): Single<Float>

  fun saveBaseCurrency(currency: Currency): Completable
  fun saveBaseValue(value: Float): Completable
}

internal class DefaultConverterRepository(
  private val preferences: ConverterPreferences,
  private val api: Api
) : ConverterRepository {

  override fun latestRates(currency: Currency): Single<List<Rate>> =
    api
      .latestRates(currency.name)
      .map { it.toRatesList() }

  override fun baseCurrency(default: Currency): Single<Currency> =
    Single.fromCallable {
      Currency.valueOf(preferences.getBaseCurrency(default.name))
    }

  override fun baseValue(default: Float): Single<Float> =
    Single.fromCallable {
      preferences.getBaseValue(default)
    }

  override fun saveBaseCurrency(currency: Currency): Completable =
    Completable.fromAction {
      preferences.saveBaseCurrency(currency.name)
    }

  override fun saveBaseValue(value: Float): Completable =
    Completable.fromAction {
      preferences.saveBaseValue(value)
    }

  private fun LatestRates.toRatesList(): List<Rate> {
    val ratesList = mutableListOf<Rate>()

    rates?.apply {
      AUD?.let { ratesList.add(Rate(Currency.AUD, it)) }
      BRL?.let { ratesList.add(Rate(Currency.BRL, it)) }
      CAD?.let { ratesList.add(Rate(Currency.CAD, it)) }
      CNF?.let { ratesList.add(Rate(Currency.CNF, it)) }
      CNY?.let { ratesList.add(Rate(Currency.CNY, it)) }
      CZK?.let { ratesList.add(Rate(Currency.CZK, it)) }
      DKK?.let { ratesList.add(Rate(Currency.DKK, it)) }
      EUR?.let { ratesList.add(Rate(Currency.EUR, it)) }
      GBP?.let { ratesList.add(Rate(Currency.GBP, it)) }
      HKD?.let { ratesList.add(Rate(Currency.HKD, it)) }
      HRK?.let { ratesList.add(Rate(Currency.HRK, it)) }
      HUF?.let { ratesList.add(Rate(Currency.HUF, it)) }
      ILS?.let { ratesList.add(Rate(Currency.ILS, it)) }
      INR?.let { ratesList.add(Rate(Currency.INR, it)) }
      ISK?.let { ratesList.add(Rate(Currency.ISK, it)) }
      JPY?.let { ratesList.add(Rate(Currency.JPY, it)) }
      KRW?.let { ratesList.add(Rate(Currency.KRW, it)) }
      MXN?.let { ratesList.add(Rate(Currency.MXN, it)) }
      MYR?.let { ratesList.add(Rate(Currency.MYR, it)) }
      NOK?.let { ratesList.add(Rate(Currency.NOK, it)) }
      NZD?.let { ratesList.add(Rate(Currency.NZD, it)) }
      PHP?.let { ratesList.add(Rate(Currency.PHP, it)) }
      PLN?.let { ratesList.add(Rate(Currency.PLN, it)) }
      RON?.let { ratesList.add(Rate(Currency.RON, it)) }
      RUB?.let { ratesList.add(Rate(Currency.RUB, it)) }
      SEK?.let { ratesList.add(Rate(Currency.SEK, it)) }
      SGD?.let { ratesList.add(Rate(Currency.SGD, it)) }
      THB?.let { ratesList.add(Rate(Currency.THB, it)) }
      TRY?.let { ratesList.add(Rate(Currency.TRY, it)) }
    }

    return ratesList
  }
}