package com.revolut.currencies.converter.data

import android.content.SharedPreferences
import androidx.core.content.edit

interface ConverterPreferences {
  fun saveBaseCurrency(currency: String)
  fun getBaseCurrency(default: String): String

  fun saveBaseValue(value: Float)
  fun getBaseValue(default: Float): Float
}

internal class DefaultConverterPreferences(
  private val sharedPreferences: SharedPreferences
) : ConverterPreferences {
  companion object {
    private const val KEY_BASE_CURRENCY = "base_currency"
    private const val KEY_BASE_VALUE = "base_value"
  }

  override fun saveBaseCurrency(currency: String) =
    sharedPreferences.edit {
      putString(KEY_BASE_CURRENCY, currency)
    }

  override fun getBaseCurrency(default: String): String =
    sharedPreferences.getString(KEY_BASE_CURRENCY, default) ?: default

  override fun saveBaseValue(value: Float) =
    sharedPreferences.edit {
      putFloat(KEY_BASE_VALUE, value)
    }

  override fun getBaseValue(default: Float): Float =
    sharedPreferences.getFloat(KEY_BASE_VALUE, default)
}