package com.revolut.currencies.utils

import android.util.Log
import io.reactivex.exceptions.UndeliverableException
import io.reactivex.functions.Consumer
import java.io.IOException

class RxJavaErrorHandler : Consumer<Throwable> {
  override fun accept(t: Throwable) {
    var throwable: Throwable? = t
    if (throwable is UndeliverableException) {
      throwable = throwable.cause
    }

    // Throwable belongs to a set of "fatal" error varieties. Values here derived from https://github.com/ReactiveX/RxJava/issues/748#issuec..
    // See also io.reactivex.exceptions.Exceptions.throwIfFatal
    if (throwable is VirtualMachineError || throwable is ThreadDeath || throwable is LinkageError) {
      Log.w("RxJava", "Undeliverable exception received", throwable)
      Thread.currentThread().uncaughtExceptionHandler.uncaughtException(Thread.currentThread(), throwable)
      return
    }

    if (throwable is IOException) {
      // Network issue or API that throws on cancellation
      return
    }
    if (throwable is InterruptedException) {
      // Blocking code was interrupted by a dispose call
      return
    }
    if (throwable is NullPointerException || throwable is IllegalArgumentException) {
      // Likely a bug in the application
      Log.w("RxJava", "Undeliverable exception received", throwable)
      Thread.currentThread().uncaughtExceptionHandler.uncaughtException(Thread.currentThread(), throwable)
      return
    }
    if (throwable is IllegalStateException) {
      // Likely a bug in RxJava or in a custom operator
      Log.w("RxJava", "Undeliverable exception received", throwable)
      Thread.currentThread().uncaughtExceptionHandler.uncaughtException(Thread.currentThread(), throwable)
      return
    }

    Log.w("RxJava", "Undeliverable exception received", throwable)
  }
}