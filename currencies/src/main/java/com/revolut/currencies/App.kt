package com.revolut.currencies

import android.app.Activity
import android.app.Application
import com.revolut.currencies.di.DaggerAppComponent
import com.revolut.currencies.utils.RxJavaErrorHandler
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasActivityInjector
import io.reactivex.plugins.RxJavaPlugins
import javax.inject.Inject


class App : Application(), HasActivityInjector {

  @Inject
  lateinit var activityDispatchingAndroidInjector: DispatchingAndroidInjector<Activity>

  override fun onCreate() {
    super.onCreate()

    DaggerAppComponent
      .factory()
      .create(this)
      .inject(this)

    RxJavaPlugins.setErrorHandler(RxJavaErrorHandler())
  }

  override fun activityInjector(): AndroidInjector<Activity> =
    activityDispatchingAndroidInjector
}
