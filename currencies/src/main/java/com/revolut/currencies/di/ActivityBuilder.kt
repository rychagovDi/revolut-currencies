package com.revolut.currencies.di

import com.revolut.currencies.converter.di.ConverterModule
import com.revolut.currencies.converter.ui.ConverterActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
interface ActivityBuilder {

  @ContributesAndroidInjector(modules = [ConverterModule::class])
  fun bindCurrenciesActivity(): ConverterActivity
}