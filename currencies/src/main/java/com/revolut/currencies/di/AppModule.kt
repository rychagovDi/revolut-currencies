package com.revolut.currencies.di

import android.content.Context
import android.content.SharedPreferences
import dagger.Module
import dagger.Provides

@Module
object AppModule {
  private const val SHARED_PREFERENCES_NAME = "revolut_preferences"

  @JvmStatic
  @Provides
  fun provideSharedPreferences(context: Context): SharedPreferences = context.getSharedPreferences(SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE)
}