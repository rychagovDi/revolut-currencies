package com.revolut.currencies.di

import android.content.Context
import com.revolut.currencies.App
import com.revolut.currencies.api.di.ApiModule
import com.revolut.currencies.network.di.NetworkModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import javax.inject.Singleton

@Singleton
@Component(
  modules = [
    AndroidInjectionModule::class,
    ActivityBuilder::class,
    AppModule::class,
    ApiModule::class,
    NetworkModule::class
  ]
)
interface AppComponent {
  @Component.Factory
  interface Factory {
    fun create(@BindsInstance applicationContext: Context): AppComponent
  }

  fun inject(app: App)
}