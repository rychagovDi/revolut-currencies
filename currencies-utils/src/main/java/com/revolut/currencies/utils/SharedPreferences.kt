package com.revolut.currencies.utils

import android.content.SharedPreferences

internal fun SharedPreferences.edit(performOperations: SharedPreferences.Editor.() -> Unit) {
  val editor = edit()
  editor.performOperations()
  editor.apply()
}