package com.revolut.currencies.utils

import io.reactivex.*
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

fun <T> Flowable<T>.applyUiSchedulers(): Flowable<T> =
  this
    .subscribeOn(Schedulers.io())
    .observeOn(AndroidSchedulers.mainThread())

fun <T> Observable<T>.applyUiSchedulers(): Observable<T> =
  this
    .subscribeOn(Schedulers.io())
    .observeOn(AndroidSchedulers.mainThread())

fun <T> Single<T>.applyUiSchedulers(): Single<T> =
  this
    .subscribeOn(Schedulers.io())
    .observeOn(AndroidSchedulers.mainThread())

fun <T> Maybe<T>.applyUiSchedulers(): Maybe<T> =
  this
    .subscribeOn(Schedulers.io())
    .observeOn(AndroidSchedulers.mainThread())

fun Completable.applyUiSchedulers(): Completable =
  this
    .subscribeOn(Schedulers.io())
    .observeOn(AndroidSchedulers.mainThread())