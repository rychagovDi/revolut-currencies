package com.revolut.currencies.utils

import android.app.Activity
import android.view.View
import android.view.inputmethod.InputMethodManager

fun View.hideKeyboard(callback: (() -> Unit)? = null) {
  val imm = context.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
  clearFocus()
  imm.hideSoftInputFromWindow(rootView.windowToken, 0)
  callback?.invoke()
}

fun View.showKeyboard(callback: (() -> Unit)? = null) {
  val imm = context.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
  requestFocus()
  imm.showSoftInput(this, InputMethodManager.SHOW_FORCED)
  callback?.invoke()
}

