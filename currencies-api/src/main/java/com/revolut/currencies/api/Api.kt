package com.revolut.currencies.api

import com.revolut.currencies.api.data.LatestRates
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface Api {
  @GET("/latest")
  fun latestRates(
    @Query("base") base: String
  ): Single<LatestRates>
}