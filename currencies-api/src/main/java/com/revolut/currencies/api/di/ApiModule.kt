package com.revolut.currencies.api.di

import com.revolut.currencies.api.Api
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit

@Module
object ApiModule {
  @JvmStatic
  @Provides
  fun provideApi(retrofit: Retrofit): Api = retrofit.create(Api::class.java)
}