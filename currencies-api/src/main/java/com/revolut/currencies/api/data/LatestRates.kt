package com.revolut.currencies.api.data

import java.util.*

data class LatestRates(
  val base: String?,
  val date: Date?,
  val rates: Rates?
)